package dawid.fbauthtest;

import java.security.Principal;
import java.util.Map;

import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Controller {
	
	private final UserRepository userRepository;
	
	@RequestMapping("/user")
	public Principal user(OAuth2Authentication principal) {
		return principal;
	}
	
	@RequestMapping("/register")
	public String register(OAuth2Authentication authentication) {
		userRepository.save(userFrom(authentication));
		return "registered!";
	}
	
	private static User userFrom(OAuth2Authentication authentication) {
		Map<String, String> details = (Map<String, String>) authentication.getUserAuthentication().getDetails();
		return User.builder()
				.id(Long.parseLong(details.get("id")))
				.name(details.get("name"))
				.build();
	}
}
